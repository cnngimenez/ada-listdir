--  paths.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------
with Ada.Directories;
with Ada.Calendar;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package Paths is
    type Path_Type is tagged private;
    --  A representation of a Path.
    --
    --  A Directory_Entry is a limited type, and cannot be reused.  This is not
    --  limited and can be copied.  It can also be used as an entry Parameter.

    procedure Initialise (Path : in out Path_Type;
        Full_Name : String;
        Simple_Name : String;
        Kind : Ada.Directories.File_Kind;
        Modification_Time : Ada.Calendar.Time;
        Size : Ada.Directories.File_Size);
    function Get_Simple_Name (Path : Path_Type) return String;
    function Get_Full_Name (Path : Path_Type) return String;
    function Get_Kind (Path : Path_Type) return Ada.Directories.File_Kind;
    function Get_Size (Path : Path_Type) return Ada.Directories.File_Size;
    function Get_Modification_Time (Path : Path_Type) return Ada.Calendar.Time;
    function Is_Invalid (Path : Path_Type) return Boolean;

    Invalid_Path : constant Path_Type;

    function To_Path_Type
        (Directory_Entry : Ada.Directories.Directory_Entry_Type)
        return Path_Type;

private
    type Path_Type is tagged record
        Simple_Name : Unbounded_String;
        Full_Name : Unbounded_String;
        Kind : Ada.Directories.File_Kind;
        Size : Ada.Directories.File_Size;
        Modification_Time : Ada.Calendar.Time;
    end record;

    Invalid_Time : constant Ada.Calendar.Time :=
        Ada.Calendar.Time_Of (1901, 1, 1);

    Invalid_Path : constant Path_Type :=
        (Simple_Name => Null_Unbounded_String,
         Full_Name => Null_Unbounded_String,
         Kind => Ada.Directories.Directory,
         Size => 0,
         Modification_Time => Invalid_Time);
end Paths;
