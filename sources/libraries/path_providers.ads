--  path_providers.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Directories;
with Ada.Containers.Vectors;
with Paths;
use Paths;

package Path_Providers is

    task type Path_Provider is
    --  Do a depth-first scanning of a Directory.
    --  Collect several files and their information for later use.

        entry Start (Path : String);
        --  Start retrieving file and directories from the given Path.

        entry Get_Path (Path : out Path_Type);
        --  Get one path information.

    end Path_Provider;

private

    Report_Mode : constant Boolean := True;
    --  If Report_Mode is True, report the progress to standard out.
    --  It is intended to skip code in compilation time if this is False.

    type Search_Type_Access is access Ada.Directories.Search_Type;

    package Search_Stack_Pack is new Ada.Containers.Vectors
        (Index_Type => Positive,
         Element_Type => Search_Type_Access);
    subtype Search_Stack_Vector is Search_Stack_Pack.Vector;

end Path_Providers;
