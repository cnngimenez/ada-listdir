--  path_providers.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Exceptions;
use Ada.Exceptions;
with Ada.Directories;
use Ada.Directories;
with Ada.Text_IO;
use Ada.Text_IO;

package body Path_Providers is

    task body Path_Provider is
        Previous_Directory : constant String := Current_Directory;
        File_Count : Natural := 0;
        Directory_Entry : Directory_Entry_Type;
        Current_Search : Search_Type_Access := new Search_Type;
        Search_Stack : Search_Stack_Vector;
        Exit_Loop : Boolean := False;

        function Is_Ignored (Directory_Entry : Directory_Entry_Type)
            return Boolean;
        procedure Ignore_Particular_Entries;
        procedure Report_File_Count;
        procedure Report_Directory_Entry (Path : String);

        procedure Ignore_Particular_Entries is
        begin
            while (Simple_Name (Directory_Entry) = "."
                  or else Simple_Name (Directory_Entry) = "..")
                  and then More_Entries (Current_Search.all)
            loop
                Get_Next_Entry (Current_Search.all, Directory_Entry);
            end loop;
        end Ignore_Particular_Entries;

        function Is_Ignored (Directory_Entry : Directory_Entry_Type)
            return Boolean is
        begin
            return Simple_Name (Directory_Entry) = "."
                   or else Simple_Name (Directory_Entry) = "..";
        end Is_Ignored;

        procedure Report_Directory_Entry (Path : String) is
        begin
            Put_Line ("Entering directory: " & Path);
            Flush;
        end Report_Directory_Entry;

        procedure Report_File_Count is
        begin
            if File_Count mod 100 = 0 then
                Put_Line ("Files up to now: "
                    & File_Count'Image);
                Flush;
            end if;
        end Report_File_Count;

    begin

        accept Start (Path : String) do
            Set_Directory (Path);
        end Start;

        Start_Search (Current_Search.all, ".", "*");
        Search_Stack.Append (Current_Search);

        while not Search_Stack.Is_Empty loop
            while not More_Entries (Current_Search.all) loop
                --  Go to the parent directory until finding one with entries.
                End_Search (Current_Search.all);
                Search_Stack.Delete_Last;

                if not Search_Stack.Is_Empty then
                    Current_Search := Search_Stack.Last_Element;  --  Pop
                else
                    --  No more parent directories! end everything now!
                    Exit_Loop := True;
                    exit;
                end if;
            end loop;

            exit when Exit_Loop;

            Get_Next_Entry (Current_Search.all, Directory_Entry);

            File_Count := File_Count + 1;

            if Report_Mode then
                Report_File_Count;
            end if;

            select
                accept Get_Path (Path : out Path_Type) do
                    Path := To_Path_Type (Directory_Entry);
                end Get_Path;
            or
                terminate;
            end select;

            if Kind (Directory_Entry) = Directory
               and then not Is_Ignored (Directory_Entry)
            then
                if Report_Mode then
                    Report_Directory_Entry (Full_Name (Directory_Entry));
                end if;

                Current_Search := new Search_Type;
                Start_Search (Current_Search.all,
                              Full_Name (Directory_Entry), "*");
                Search_Stack.Append (Current_Search);
            end if;

        end loop;

        End_Search (Current_Search.all);
        Set_Directory (Previous_Directory);

        --  The last accept just in case another task is doing a rendez-vous.
        loop
            select
                accept Get_Path (Path : out Path_Type) do
                    Path := Invalid_Path;
                end Get_Path;
            or
                terminate;
            end select;
        end loop;

        exception
        when Error : others =>
            Put ("Error in Provider task:");
            Put_Line (Exception_Message (Error));
            Put_Line (Exception_Information (Error));
    end Path_Provider;
end Path_Providers;
