--  hashes.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Streams;
with SipHash;

package Hashes is

    function SipHash_File (Path : String) return String;
    --  Read the file and calculate the hash using SipHash algorithm.

    --  procedure Blake2s_File (Path : String; Hash : out String);

    --  procedure Xxhash_File (Path : String; Hash : out String);

private
    procedure Stream_Element_To_U8 (
                Stream_Data : Ada.Streams.Stream_Element_Array;
                Data : out SipHash.Byte_Sequence);

end Hashes;
