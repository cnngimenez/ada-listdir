--  hashes.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Streams.Stream_IO;

with SipHash.PRF;

package body Hashes is
    package Siphash24 is new SipHash.PRF (
        Nb_Compression_Rounds => 2,
        Nb_Finalization_Rounds => 4);
    package U8io renames Ada.Streams.Stream_IO;

    --  Block_Size : constant Ada.Streams.Stream_Element_Offset := 1048576;
    --  1048576B = 1MB
    --  U64_Block_Size : constant SipHash.U64 := 1048576;
    --  Must be the same as Block_Size.

    Block_Size : constant Ada.Streams.Stream_Element_Offset := 1024;
    --  1KB = 1024B
    U64_Block_Size : constant SipHash.U64 := 1024;
    --  Must be the same as Block_Size.

    SipHash_Default_Key : constant SipHash.Key_Type := (
     16#00#, 16#01#, 16#02#, 16#03#, 16#04#, 16#05#, 16#06#, 16#07#,
     16#08#, 16#09#, 16#0a#, 16#0b#, 16#0c#, 16#0d#, 16#0e#, 16#0f#);
    --  Copied from Tests package in SipHas library.

    function SipHash_File (Path : String) return String is
        File : U8io.File_Type;
        Stream_Data : Ada.Streams.Stream_Element_Array (1 .. Block_Size);
        Last : Ada.Streams.Stream_Element_Offset;

        Data : SipHash.Byte_Sequence (1 .. U64_Block_Size);
        Hash : Siphash24.Object := Siphash24.Initialize (SipHash_Default_Key);
        Result : SipHash.U64;
    begin
        U8io.Open (File, U8io.In_File, Path);
        while not U8io.End_Of_File (File) loop
            U8io.Read (File, Stream_Data, Last);

            Stream_Element_To_U8 (Stream_Data, Data);
            Siphash24.Update (Hash, Data);
        end loop;
        U8io.Close (File);

        Siphash24.Finalize (Hash, Result);
        return Result'Image;
    end SipHash_File;

    procedure Stream_Element_To_U8 (
                Stream_Data : Ada.Streams.Stream_Element_Array;
                Data : out SipHash.Byte_Sequence) is
    begin
        for I in Stream_Data'Range loop
            Data (SipHash.U64 (I)) := SipHash.U8 (Stream_Data (I));
        end loop;
    end Stream_Element_To_U8;

end Hashes;
