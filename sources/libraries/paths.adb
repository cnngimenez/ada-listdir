--  paths.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package body Paths is
    function Get_Full_Name (Path : Path_Type) return String is
        (To_String (Path.Full_Name));

    function Get_Kind (Path : Path_Type) return Ada.Directories.File_Kind is
        (Path.Kind);

    function Get_Modification_Time (Path : Path_Type) return Ada.Calendar.Time
        is (Path.Modification_Time);

    function Get_Simple_Name (Path : Path_Type) return String is
        (To_String (Path.Simple_Name));

    function Get_Size (Path : Path_Type) return Ada.Directories.File_Size is
        (Path.Size);

    procedure Initialise (Path : in out Path_Type;
        Full_Name : String;
        Simple_Name : String;
        Kind : Ada.Directories.File_Kind;
        Modification_Time : Ada.Calendar.Time;
        Size : Ada.Directories.File_Size) is
    begin
        Path.Simple_Name := To_Unbounded_String (Simple_Name);
        Path.Full_Name := To_Unbounded_String (Full_Name);
        Path.Kind := Kind;
        Path.Modification_Time := Modification_Time;
        Path.Size := Size;
    end Initialise;

    function Is_Invalid (Path : Path_Type) return Boolean is
        (Path.Simple_Name = Null_Unbounded_String);

    function To_Path_Type
        (Directory_Entry : Ada.Directories.Directory_Entry_Type)
        return Path_Type
    is
        use Ada.Directories;
        Path : Path_Type;
    begin
        Path.Initialise (
            Full_Name => Full_Name (Directory_Entry),
            Simple_Name => Simple_Name (Directory_Entry),
            Kind => Kind (Directory_Entry),
            Modification_Time => Modification_Time (Directory_Entry),
            Size => Size (Directory_Entry));

        return Path;
    end To_Path_Type;

end Paths;
