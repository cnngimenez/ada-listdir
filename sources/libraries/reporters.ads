--  reporters.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------
with Ada.Calendar;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package Reporters is

    --  A reporter task shows to output a message keeping track of the last
    --  one  provided by each task.
    --
    --  Each task is identified by a number: Task_Num. Reporters keep a record
    --  of new and old task numbers Used.
    --
    --  Also, reporters keep track of the last message time a task has
    --  reported.
    task type Reporter_Task is
        entry Report (Task_Num : Positive; Message : String);
    end Reporter_Task;

private

    type Report_Type is record
        Task_Num : Positive;
        Message : Unbounded_String;
        Time : Ada.Calendar.Time;
    end record;

    package Report_Vector_Pack is new Ada.Containers.Vectors
        (Index_Type => Positive,
         Element_Type => Report_Type);
    subtype Report_Vector is Report_Vector_Pack.Vector;

end Reporters;
