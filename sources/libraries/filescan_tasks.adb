--  filescan_tasks.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Exceptions;
use Ada.Exceptions;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Directories;
use Ada.Directories;
with Paths;
use Paths;
with Hashes;

package body Filescan_Tasks is
    task body Filescan_Task is
        Provider : access Path_Provider;
        Path : Path_Type;
        Count : Natural := 0;
        Task_Num : Positive;

        procedure Print_Path;

        procedure Print_Path is
        begin
            if Path.Get_Simple_Name = "."
               or else Path.Get_Simple_Name = ".."
            then
                return;
            end if;

            Count := Count + 1;
            Put ("FS " & Task_Num'Image & ">" & Count'Image & ": ");
            Put_Line (Path.Get_Simple_Name);
            if Path.Get_Kind = Ordinary_File then
                Put_Line (Hashes.SipHash_File (Path.Get_Full_Name));
            end if;
            Flush;
        end Print_Path;
    begin
        accept Start (The_Provider : aliased in out Path_Provider;
                      My_Tasknum : Positive)
        do
            Provider := The_Provider'Unchecked_Access;
            Task_Num := My_Tasknum;
        end Start;

        Provider.all.Get_Path (Path);

        while not Path.Is_Invalid loop
            Print_Path;
            Provider.all.Get_Path (Path);
        end loop;

        Put_Line ("FS "  & Task_Num'Image & "> Task ended with "
                  & Count'Image & " files and directories processed.");
        exception
        when Error : others =>
            Put ("Error in Filescan task "
                 & Task_Num'Image & ": ");
            Put_Line (Exception_Message (Error));
            Put_Line (Exception_Information (Error));
    end Filescan_Task;
end Filescan_Tasks;
