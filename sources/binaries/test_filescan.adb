--  test_filescan.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;

with Filescan_Tasks;
use Filescan_Tasks;
with Path_Providers;
use Path_Providers;

procedure Test_Filescan is
    Provider : aliased Path_Provider;
    Filescan : Filescan_Task;
    Filescan2 : Filescan_Task;

    procedure Show_Help;

    procedure Show_Help is
    begin
        Put_Line ("Synopsis:");
        Put_Line ("    test_filescan PATH");
    end Show_Help;

begin
    if Ada.Command_Line.Argument_Count /= 1 then
        Show_Help;
        return;
    end if;

    Provider.Start (Ada.Command_Line.Argument (1));
    Filescan.Start (Provider, 1);
    Filescan2.Start (Provider, 2);
end Test_Filescan;
