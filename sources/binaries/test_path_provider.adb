--  test_path_provider.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Directories;
with Ada.Command_Line;
with Ada.Calendar.Formatting;
with Path_Providers;
with Paths;
use Paths;

procedure Test_Path_Provider is
    Long_Output : constant Boolean := False;

    Provider : Path_Providers.Path_Provider;
    Count : Natural := 0;

    procedure Put_Path (Path : Paths.Path_Type);
    procedure Show_Help;
    procedure Count_If_Not_Ignored (Path : Path_Type);

    procedure Count_If_Not_Ignored (Path : Path_Type) is
    begin
        if Path.Get_Simple_Name /= "."
           and then Path.Get_Simple_Name /= ".."
        then
            Count := Count + 1;
        end if;
    end Count_If_Not_Ignored;

    procedure Put_Path (Path : Paths.Path_Type) is
        use Ada.Directories;
    begin
        case Path.Get_Kind is
        when Ordinary_File =>
            Put ("\-f-> ");
        when Special_File =>
            Put ("\-s-> ");
        when Directory =>
            Put ("\-d-> ");
        end case;
        if Long_Output then
            Put_Line (Path.Get_Simple_Name & ","
                  & Path.Get_Full_Name & ","
                  & Path.Get_Kind'Image & ","
                  & Path.Get_Size'Image & ","
                  & Ada.Calendar.Formatting.Image
                    (Path.Get_Modification_Time));
        else
            Put_Line (Path.Get_Full_Name);
        end if;
    end Put_Path;

    procedure Show_Help is
    begin
        Put_Line ("Synopsis:");
        Put_Line ("    test_path_provider PATH");
        New_Line;
        Put_Line ("This program test the Path_Provider task. In this case, "
            & "it should list all files in a directory recursively.");
        Put_Line ("In implementation, it creates a task and request to it "
            & " paths until an invalid one is returned.");
    end Show_Help;

    Path : Paths.Path_Type;

begin
    if Ada.Command_Line.Argument_Count /= 1 then
        Show_Help;
        return;
    end if;

    Provider.Start (Ada.Command_Line.Argument (1));
    Put_Line ("Task started");

    Provider.Get_Path (Path);
    while not Path.Is_Invalid loop
        Count_If_Not_Ignored (Path);
        Put (Count'Image & ":");
        Put_Path (Path);
        Provider.Get_Path (Path);
    end loop;

end Test_Path_Provider;
