--  listdir_simple.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Directories;
use Ada.Directories;
with Ada.Calendar.Formatting;

with Hashes;

procedure Listdir_Simple is
    procedure Show_Help;
    procedure Process_Directory (Path : String);
    procedure Process_Directory_Int (Directory_Entry : Directory_Entry_Type);
    procedure Put_Directory_Entry (Directory_Entry : Directory_Entry_Type);
    function Path_Is_Correct (Path : String) return Boolean;
    function Simple_Path (Path : String) return String;

    Output_File : File_Type;
    File_Count : Natural := 0;

    function Path_Is_Correct (Path : String) return Boolean is
    begin
        return Ada.Directories.Exists (Path) and then
            Ada.Directories.Kind (Path) = Directory;
    end Path_Is_Correct;

    procedure Process_Directory (Path : String) is
        Previous_Directory : constant String :=
            Ada.Directories.Current_Directory;
    begin
        Put_Line (Output_File,
            """path"",""type"",""size_bytes"",""Modification_Date"","
            & """Siphash""");

        Ada.Directories.Set_Directory (Path);
        Put_Line ("Chdir: " & Path);
        Ada.Directories.Search (".", "*",
            Process => Process_Directory_Int'Access);
        Ada.Directories.Set_Directory (Previous_Directory);
        Put_Line ("Chdir: " & Previous_Directory);
    end Process_Directory;

    procedure Process_Directory_Int (Directory_Entry : Directory_Entry_Type) is
        Name : constant String := Simple_Name (Directory_Entry);
    begin
        if Name = "." or else Name = ".." then
            return;
        end if;

        Put_Directory_Entry (Directory_Entry);
        if File_Count mod 100 = 0 then
            Put_Line ("Files up to now: " & File_Count'Image);
        end if;

        if Kind (Directory_Entry) = Directory then
            Put_Line ("Entering diretory:" & Full_Name (Directory_Entry));
            Put_Line ("Files up to now: " & File_Count'Image);
            Flush;

            Ada.Directories.Search (Full_Name (Directory_Entry), "*",
                Process => Process_Directory_Int'Access);
        end if;
    end Process_Directory_Int;

    procedure Put_Directory_Entry (Directory_Entry : Directory_Entry_Type) is
        Path : constant String := Simple_Path (Full_Name (Directory_Entry));
    begin
        Put (Output_File, """" & Path & ""","""
            & Kind (Directory_Entry)'Image & ""","
            & Size (Directory_Entry)'Image & ","""
            & Ada.Calendar.Formatting.Image
                (Modification_Time (Directory_Entry))
            & """,""");

        if Kind (Directory_Entry) = Ordinary_File then
            File_Count := File_Count + 1;

            Put_Line (Output_File, Hashes.SipHash_File (Path)
                & """");
        else
            Put_Line (Output_File, """");
        end if;
    end Put_Directory_Entry;

    procedure Show_Help is
    begin
        Put_Line ("Synopsis:");
        New_Line;
        Put_Line ("    listdir PATH OUTPUT_PATH");
        New_Line;
        Put_Line ("List directories and their files recursively from PATH."
            & "Report all file with their size.");
    end Show_Help;

    function Simple_Path (Path : String) return String is
    begin
        if Path (Path'First .. Path'First + 1) = "./" then
            return Path (Path'First + 2 .. Path'Last);
        else
            return Path;
        end if;
    end Simple_Path;

begin
    if Argument_Count /= 2 then
        Show_Help;
        Set_Exit_Status (Failure);
        return;
    end if;

    if not Path_Is_Correct (Argument (1)) then
        Put_Line ("The path provided is not correct: It must be a directory.");
        Set_Exit_Status (Failure);
        return;
    end if;

    Create (Output_File, Out_File, Argument (2));

    Process_Directory (Argument (1));

    Close (Output_File);

end Listdir_Simple;
